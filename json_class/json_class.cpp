﻿#include <iostream>
#include "json_pair.h"

std::string json_example();

int main()
{
    std::cout << "Hello World!\n";

	try
	{

		json_pair <int> p1("string1", 12);
		json_pair <double> p2("string2", 12.56);
		json_pair <std::string> p3("string3", "str3");
		json_pair <std::string> p4("\"string4\":\"str4\"");
		json_pair <int> p5("\"string5\":15");

		std::cout << p1.name << " " << p1.value << std::endl;
		std::cout << p2.name << " " << p2.value << std::endl;
		std::cout << p3.name << " " << p3.value << std::endl;
		std::cout << p4.name << " " << p4.value << std::endl;
		std::cout << p5.name << " " << p5.value << std::endl;


		Context cntxt = Context(json_example());
		std::cout << "Print all Context objects:" << std::endl;
		for (int i = 0; i < cntxt.objects.size(); i++)
		{
			std::cout << cntxt.objects[i].object << std::endl;
		}

		int cntrl = cntxt.del_from_Context(0);
		if (cntrl == 0)
		{
			std::cout << "objects #0 was deleted from Context" << std::endl;
		}
		std::string temp = "{\"alice\":true}";

		Context temp_cntxt = Context(temp);

		cntrl = cntxt.add_to_Context(temp_cntxt);
		if (cntrl == 0)
		{
			std::cout << temp << " was added to Context" << std::endl;
		}

		cntrl = cntxt.fake_serialization(&temp);
		if (cntrl == 0)
		{
			std::cout << "Resulted Context:" << std::endl;
			std::cout << temp << std::endl;
		}


		cntrl = cntxt.get_part_from_Context(3, &temp);
		if (cntrl == 0)
		{
			std::cout << "object #3 is array:" << std::endl;
			std::cout << temp << std::endl;
		}
				
		Context temp_cntxt1 = Context(temp);

		std::string temp1;
		cntrl = temp_cntxt1.get_part_from_Context(1, &temp1);
		if (cntrl == 0)
		{
			std::cout << "object #1 in array:" << std::endl;
			std::cout << temp1 << std::endl;
		}
		Context temp_cntxt2 = Context(temp1);
		int num = temp_cntxt2.get_num_for_name("int");
		int test_int;
		cntrl = temp_cntxt2.get_value_from_Context(num, &test_int);
		if (cntrl == 0)
		{
			std::cout << "test_int= " << test_int << std::endl;
		}

		std::string temp2;
		double test_double;
		cntrl = cntxt.get_value_from_Context(0, &test_double);
		if (cntrl == 0)
		{
			std::cout << "test_double= " << test_double << std::endl;
		}

		std::cout << "sum= " << test_double+test_int << std::endl;

		bool test_bool;
		cntrl = cntxt.get_value_from_Context(cntxt.get_num_for_name("alice"), &test_bool);
		if (cntrl == 0)
		{
			if (test_bool)
			{
				std::cout << "thats OK" << std::endl;
			}
			else
			{
				std::cout << "value = false" << std::endl;
			}
		}

		return 0;
	}
	catch (...)
	{
		std::cout << "error. program_was_closed." << std::endl;
		return -1;
	}

	


}

std::string json_example()
{
	/*
	{23, 0.15, false, "abc",
	[
	{"int":12, "double":0.45, "bool":true, "string":"example"},
	{"int":32, "double":0.35, "bool":false, "string":"simple"}
	],
	{"int":42, "double":4.45, "bool":true, "string":"4e"},
	"second_object": {"int":52, "double":5.45, "bool":false, "string":"e5"}
	}
	*/

	std::string income_string = "{23, 0.15, false, \"abc\",";
		income_string = income_string + "[";
		income_string = income_string + "{\"int\":12, \"double\":0.45, \"bool\":true, \"string\":\"example\"},";
		income_string = income_string + "{\"int\":32, \"double\":0.35, \"bool\":false, \"string\":\"simple\"}";
		income_string = income_string + "],";
		income_string = income_string + "{\"int\":42, \"double\":4.45, \"bool\":true, \"string\":\"4e\"},";
		income_string = income_string + "\"second_object\": {\"int\":52, \"double\":5.45, \"bool\":false, \"string\":\"e5\"}";
		income_string = income_string + "}";

	return income_string;
}
