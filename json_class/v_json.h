#pragma once
#include <vector>
#include <string>
#include "json_pair.h"
#include "helpers.h"

struct type_and_object
{
	enum class json_type { json_int, json_double, json_boolean, json_string, json_object,
		json_pair_with_int, json_pair_with_double, json_pair_with_boolean, json_pair_with_string, json_pair_with_json_object,
	raw_data};

	json_type type = json_type::json_object;

	//char* link; //��������� ����� �� ������ � �������
	std::string object = "";
};


class Context
{
private:

	Context() = delete;

public:

	std::vector<type_and_object> objects;

	Context(std::string income_json);

	int add_to_Context(Context json_part);

	int get_num_for_name(std::string searched_name);

	int get_value_from_Context(int num, int* result);
	int get_value_from_Context(int num, double* result);
	int get_value_from_Context(int num, bool* result);
	int get_value_from_Context(int num, std::string* result);
	int get_part_from_Context(int num, std::string* result);

	int del_from_Context(int num);

	int fake_serialization(std::string* result);

	
};

