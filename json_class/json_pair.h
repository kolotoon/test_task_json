#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include "v_json.h"
#include "helpers.h"


template<typename T1> class json_pair
{

public:
	std::string name;
	T1 value;

	json_pair() = delete;

	json_pair(const std::string _name, const T1 _value)
	{
		name = _name;
		value = _value;
	}
	
	
	json_pair(std::string string_with_json_pair)
	{
		string_with_json_pair = helpers::del_extra_spaces_from(string_with_json_pair);

		int first_quotes_open = (int)string_with_json_pair.find_first_of("\"");
		int first_quotes_close = (int)string_with_json_pair.find_first_of(":")-1;

		int values_begin;
		int values_end;

		if (string_with_json_pair[first_quotes_close + 2] == '\"')
		{
			values_begin = first_quotes_close + 3;
			values_end = (int)string_with_json_pair.length() -1;
		}
		else
		{
			values_begin = first_quotes_close + 2;
			values_end = (int)string_with_json_pair.length();
		}

		std::string first = string_with_json_pair.substr(first_quotes_open+1, first_quotes_close - first_quotes_open - 1);
		std::string second = string_with_json_pair.substr(values_begin, values_end - values_begin);

		T1 _value;
		std::stringstream str;

		str << second;
		str >> _value;
		
		name = first;
		value = _value;

		if (second == "true")
		{
			name = first;
			value = true;
		}

	}/**/
};

