#include "helpers.h"

std::string helpers::del_extra_spaces_from(std::string string)
{
	std::string income_string = string;

	bool quotes_are_open = false;
	std::string new_str = "";
	for (int i = 0; i < income_string.length(); i++)
	{
		if (income_string[i] == '\"' && !quotes_are_open)
		{
			quotes_are_open = true;
		}
		if (income_string[i] == '\"' && quotes_are_open)
		{
			quotes_are_open = false;
		}

		if (income_string[i] != ' ' || quotes_are_open)
		{
			new_str = new_str + income_string[i];
		}
	}

	income_string = new_str;

	return income_string;
}
