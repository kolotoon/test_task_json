
#include "v_json.h"

int how_many_objects_in_json(std::string income_json)
{
	std::string income_string = helpers::del_extra_spaces_from(income_json);

	bool quotes_are_open = false;
	int how_many_objects_are_open = 0;
	int how_many_arrays_are_open = 0;
	int number_of_objects_in_json = 0;
	for (int i = 0; i < income_string.length(); i++)
	{
		if (income_string[i] == '\"'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = true;
		}

		if (income_string[i] == '\"'
			&& quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = false;
		}

		if (income_string[i] == '{'
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open++;
		}

		if (income_string[i] == '}'
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open--;
		}

		if (income_string[i] == '['
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open++;
		}

		if (income_string[i] == ']'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open--;
		}


		if (income_string[i] == ','
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			if (number_of_objects_in_json == 0)
			{
				number_of_objects_in_json++;
			}
			number_of_objects_in_json++;
		}

	}


	if (number_of_objects_in_json == 0 && income_string.length() > 2)
	{
		number_of_objects_in_json = 1;
	}

	return number_of_objects_in_json;
}

std::string get_json_part_by_num(std::string income_json, int income_num)
{
	
	std::string income_string = income_json;

	income_string = helpers::del_extra_spaces_from(income_string);

	int number_of_objects_in_json = how_many_objects_in_json(income_string);


	int index_of_split_objects = -1;
	int old_index_of_split_objects = -1;
	bool quotes_are_open = false;
	int how_many_objects_are_open = 0;
	int how_many_arrays_are_open = 0;
	int k = -1;
	for (int i = 0; i < income_string.length(); i++)
	{
		if (income_string[i] == '\"'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = true;
		}

		if (income_string[i] == '\"'
			&& quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = false;
		}

		if (income_string[i] == '{'
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open++;
		}

		if (income_string[i] == '}'
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open--;
		}

		if (income_string[i] == '['
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open++;
		}

		if (income_string[i] == ']'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open--;
		}


		if ((income_string[i] == ',')
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			old_index_of_split_objects = index_of_split_objects;
			index_of_split_objects = i;
			k++;
		}

		if (i == income_string.length() - 1)
		{
			k++;
			if (index_of_split_objects == 0)
			{
				index_of_split_objects = (int)income_string.length();
			}
			else
			{
				old_index_of_split_objects = index_of_split_objects;
				index_of_split_objects = (int)income_string.length();
			}
		}

		if (k == income_num)
		{
			std::string result = income_string.substr(old_index_of_split_objects + 1, (index_of_split_objects - old_index_of_split_objects - 1));
			return result;
		}
	}

	return "";
}

type_and_object parse_string_to_object(std::string string_for_parse)
{
	string_for_parse = helpers::del_extra_spaces_from(string_for_parse);

	type_and_object new_obj = type_and_object();

	int number_of_objects_in_json = how_many_objects_in_json(string_for_parse);

	if (number_of_objects_in_json > 1)
	{
		new_obj.type = type_and_object::json_type::raw_data;
		new_obj.object = string_for_parse;
		return new_obj;
	}

	if (string_for_parse[0] == '[' && string_for_parse[string_for_parse.length() - 1] == ']')
	{
		new_obj.type = type_and_object::json_type::json_object;
		string_for_parse[0] = '{';
		string_for_parse[string_for_parse.length() - 1] = '}';
		new_obj.object = string_for_parse;
		return new_obj;
	}

	if (string_for_parse[0] == '{' && string_for_parse[string_for_parse.length() - 1] == '}')
	{
		new_obj.type = type_and_object::json_type::json_object;
		new_obj.object = string_for_parse;
		return new_obj;
	}

	if (string_for_parse == "true" || string_for_parse == "false")
	{
		new_obj.type = type_and_object::json_type::json_boolean;
		new_obj.object = string_for_parse;
		//bool b = true;//���������, ����� �� ������ � �������
		//char* c;
		//c = (char*)&b;
		//new_obj.link = c;
		return new_obj;
	}


	bool quotes_are_open = false;
	int how_many_objects_are_open = 0;
	int how_many_arrays_are_open = 0;
	for (int i = 0; i < string_for_parse.length(); i++)
	{
		if (string_for_parse[i] == '\"'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = true;
		}

		if (string_for_parse[i] == '\"'
			&& quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			quotes_are_open = false;
		}

		if (string_for_parse[i] == '{'
			&& i != 0
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open++;
		}

		if (string_for_parse[i] == '}'
			&& i != string_for_parse.length() - 1
			&& !quotes_are_open
			&& how_many_arrays_are_open == 0)
		{
			how_many_objects_are_open--;
		}

		if (string_for_parse[i] == '['
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open++;
		}

		if (string_for_parse[i] == ']'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0)
		{
			how_many_arrays_are_open--;
		}

		if (string_for_parse[i] == '.'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			try {

				double d = atof(string_for_parse.c_str());
				new_obj.object = string_for_parse;
				new_obj.type = type_and_object::json_type::json_double;
				return new_obj;
			}
			catch (...)
			{
				new_obj.type = type_and_object::json_type::raw_data;
				new_obj.object = string_for_parse;
				return new_obj;
			}


		}

		if (string_for_parse[i] == ':'
			&& !quotes_are_open
			&& how_many_objects_are_open == 0
			&& how_many_arrays_are_open == 0)
		{
			//it is pair
			type_and_object second_part_of_pair = parse_string_to_object(string_for_parse.substr(i + 1, string_for_parse.length() - (i + 1)));

			if (second_part_of_pair.type == type_and_object::json_type::json_int)
			{
				new_obj.type = type_and_object::json_type::json_pair_with_int;
				new_obj.object = string_for_parse;
				return new_obj;
			}
			else if (second_part_of_pair.type == type_and_object::json_type::json_double)
			{
				new_obj.type = type_and_object::json_type::json_pair_with_double;
				new_obj.object = string_for_parse;
				return new_obj;
			}
			else if (second_part_of_pair.type == type_and_object::json_type::json_boolean)
			{
				new_obj.type = type_and_object::json_type::json_pair_with_boolean;
				new_obj.object = string_for_parse;
				return new_obj;
			}
			else if (second_part_of_pair.type == type_and_object::json_type::json_string)
			{
				new_obj.type = type_and_object::json_type::json_pair_with_string;
				new_obj.object = string_for_parse;
				return new_obj;
			}
			else
			{
				new_obj.type = type_and_object::json_type::json_pair_with_json_object;
				new_obj.object = string_for_parse;
				return new_obj;
			}
		}
	}

	if (string_for_parse[0] == '\"' && string_for_parse[string_for_parse.length() - 1] == '\"')
	{
		new_obj.type = type_and_object::json_type::json_string;
		new_obj.object = string_for_parse.substr(1, string_for_parse.length() - 2);
		return new_obj;
	}

	try {

		int i = atoi(string_for_parse.c_str());
		new_obj.object = string_for_parse;
		new_obj.type = type_and_object::json_type::json_int;
		return new_obj;
	}
	catch (...)
	{
		new_obj.type = type_and_object::json_type::raw_data;
		new_obj.object = string_for_parse;
		return new_obj;
	}

	new_obj.type = type_and_object::json_type::raw_data;
	new_obj.object = string_for_parse;
	return new_obj;
}

Context::Context(std::string income_json)
{
	income_json = income_json.substr(1, income_json.length() - 2);

	income_json = helpers::del_extra_spaces_from(income_json);

	int number_of_objects_in_json = how_many_objects_in_json(income_json);

	for (int i = 0; i < number_of_objects_in_json; i++)
	{
		std::string part_of_income_json = get_json_part_by_num(income_json, i);
		objects.push_back(parse_string_to_object(part_of_income_json));
	}
}

int Context::add_to_Context(Context json_part)
{
	for (int i = 0; i < json_part.objects.size(); i++)
	{
		objects.push_back(json_part.objects[i]);
	}

	return 0;
}

int Context::get_num_for_name(std::string searched_name)
{
	for (int i = 0; i < objects.size(); i++)
	{
		if (objects[i].type == type_and_object::json_type::json_pair_with_boolean
			|| objects[i].type == type_and_object::json_type::json_pair_with_double
			|| objects[i].type == type_and_object::json_type::json_pair_with_int
			|| objects[i].type == type_and_object::json_type::json_pair_with_json_object
			|| objects[i].type == type_and_object::json_type::json_pair_with_string
			)
		{
			json_pair<std::string> jjj = json_pair<std::string>(objects[i].object);

			if (jjj.name == searched_name)
			{
				return i;
			}
		}
	}

	return -1;
}

int Context::get_value_from_Context(int num, int* result)
{
	if (objects[num].type == type_and_object::json_type::json_int)
	{
		int temp;
		std::stringstream strm;
		strm << objects[num].object;
		strm >> temp;
		*result = temp;
		return 0;
	}
	if (objects[num].type == type_and_object::json_type::json_pair_with_int)
	{
		json_pair<int> jjj = json_pair<int>(objects[num].object);
		*result = jjj.value;
		return 0;
	}

	return -1;
}

int Context::get_value_from_Context(int num, double* result)
{
	if (objects[num].type == type_and_object::json_type::json_double)
	{
		double temp;
		std::stringstream strm;
		strm << objects[num].object;
		strm >> temp;
		*result = temp;
		return 0;
	}
	if (objects[num].type == type_and_object::json_type::json_pair_with_double)
	{
		json_pair<double> jjj = json_pair<double>(objects[num].object);
		*result = jjj.value;
		return 0;
	}

	return -1;
}

int Context::get_value_from_Context(int num, bool* result)
{
	if (objects[num].type == type_and_object::json_type::json_boolean)
	{
		if (objects[num].object == "true")
		{
			*result = true;
		}
		else
		{
			*result = false;
		}
		return 0;
	}
	if (objects[num].type == type_and_object::json_type::json_pair_with_boolean)
	{
		json_pair<bool> jjj = json_pair<bool>(objects[num].object);
		*result = jjj.value;
		return 0;
	}

	return -1;
}

int Context::get_value_from_Context(int num, std::string* result)
{
	if (objects[num].type == type_and_object::json_type::json_string)
	{
		*result = objects[num].object;
		return 0;
	}
	if (objects[num].type == type_and_object::json_type::json_pair_with_string)
	{
		json_pair<std::string> jjj = json_pair<std::string>(objects[num].object);
		*result = jjj.value;
		return 0;
	}

	return -1;
}

int Context::get_part_from_Context(int num, std::string* result)
{
	*result = objects[num].object;

	return 0;
}


int Context::del_from_Context(int num)
{
	objects.erase(objects.begin() + num);

	return 0;
}

int Context::fake_serialization(std::string* result)
{
	std::string temp = "{";

	for (int i = 0; i < objects.size(); i++)
	{
		if (i == 0)
		{
			temp = temp + objects[i].object;
		}
		else
		{
			temp = temp + "," + objects[i].object;
		}

	}
	temp = temp + "}";

	*result = temp;

	return 0;
}

